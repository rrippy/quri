$(document).ready(function(){
	
	// buton and title initiation
	$('h4.title_two').hide();
	$('h4.title_three').hide();
	$('h4.title_four').hide();
	$('a.slide_btn_1').css('background-position', 'top right');
	$('a.quri h2').css('color', '#02aed9');
	$('#about_careers_section').hide();
	$('#about_team_section').hide();

	
	// main nav
	$('body.main_site a.home_btn').click(function(e) {
		e.preventDefault();
		// scroll document
		$.scrollTo(0,'slow');
   });
	$('body.main_site a.benefits_btn').click(function(e) {
		e.preventDefault();
		// scroll document
		$.scrollTo('#benefits_box','slow', {
			offset:-126
		});
   });
   $('body.main_site a.qcard_btn').click(function(e) {
		e.preventDefault();
		// scroll document
		$.scrollTo('#qcard_box','slow', {
			offset:-40
		});
   });
	$('body.main_site a.about_btn').click(function(e) {
		e.preventDefault();
		// scroll document
		$.scrollTo('#about_box','slow', {
			offset:-50
		});
   });
	$('body.main_site a.press_btn').click(function(e) {
		e.preventDefault();
		// scroll document
		$.scrollTo('#press_box','slow', {
			offset:-50
		});
   });
	$('body.main_site a.hire_btn').click(function(e) {
		e.preventDefault();
		// scroll document
		$.scrollTo('#contact_box','slow', {
			offset:-50
		});
   });
	
	// start timed slideshow	
	$(function(){
	  setTimeout(function(){
		autoSlideshow();
	  }, 3000)
	});
	
	// slide show buttons
	$('a.slide_btn_1').click(function(e) {
		e.preventDefault();
		// clear autoslideshow
		$('#allSlides').removeClass('notselected');
		// button state
		$('#slide_buttons a').css('background-position', 'top left').removeClass('current_slide');
		$(this).css('background-position', 'top right');
		// scroll to piece
		$('#allSlides').animate({'left':'0'}, 'slow');
		// fadein header
		$('h4.title_two').fadeOut();
		$('h4.title_three').fadeOut();
		$('h4.title_four').fadeOut();
		$('h4.title_one').delay(400).fadeIn();
   });
	
   $('a.slide_btn_2').click(function(e) {
		e.preventDefault();
		// clear autoslideshow
		$('#allSlides').removeClass('notselected');
		// button state
		$('#slide_buttons a').css('background-position', 'top left').removeClass('current_slide');
		$(this).css('background-position', 'top right');
		// scroll to piece
		$('#allSlides').animate({'left':'-554'}, 'slow');
		// fadein header
		$('h4.title_one').fadeOut();
		$('h4.title_three').fadeOut();
		$('h4.title_four').fadeOut();
		$('h4.title_two').delay(400).fadeIn();
   });
   $('a.slide_btn_3').click(function(e) {
		e.preventDefault();
		// clear autoslideshow
		$('#allSlides').removeClass('notselected');
		// button state
		$('#slide_buttons a').css('background-position', 'top left').removeClass('current_slide');
		$(this).css('background-position', 'top right');
		// scroll to piece
		$('#allSlides').animate({'left':'-1108'}, 'slow');
		// fadein header
		$('h4.title_one').fadeOut();
		$('h4.title_two').fadeOut();
		$('h4.title_four').fadeOut();
		$('h4.title_three').delay(400).fadeIn();
   });
   $('a.slide_btn_4').click(function(e) {
		e.preventDefault();
		// button state
		$('#slide_buttons a').css('background-position', 'top left').removeClass('current_slide');
		$(this).css('background-position', 'top right');
		// scroll to piece
		$('#allSlides').animate({'left':'-1662'}, 'slow');
		// fadein header
		$('h4.title_one').fadeOut();
		$('h4.title_two').fadeOut();
		$('h4.title_three').fadeOut();
		$('h4.title_four').delay(400).fadeIn();
   });
   
   // about sections
   $('a.quri').click(function(e) {
		e.preventDefault();
		// button state
		$('.subnav a h2').css('color', '#44514a');
		$('a.quri h2').css('color', '#02aed9');
		// fadein section
		$('#about_team_section').fadeOut();
		$('#about_careers_section').fadeOut();
		$('#about_quri_section').delay(400).fadeIn();
		
   });
   $('a.team').click(function(e) {
		e.preventDefault();
		// button state
		$('.subnav a h2').css('color', '#44514a');
   		$('a.team h2').css('color', '#02aed9');
		// fadein section
		$('#about_quri_section').fadeOut();
		$('#about_careers_section').fadeOut();
		$('#about_team_section').delay(400).fadeIn();
   });
   $('a.careers').click(function(e) {
		e.preventDefault();
		// button state
		$('.subnav a h2').css('color', '#44514a');
   		$('a.careers h2').css('color', '#02aed9');
		// fadein section
		$('#about_quri_section').fadeOut();
		$('#about_team_section').fadeOut();
		$('#about_careers_section').delay(400).fadeIn();
   });
   
   // press rollovers
   $('.press_btns').hover(function() {
		$(this).find('.press_hover').fadeIn();
	}, function() {
		$(this).find('.press_hover').fadeOut();
	});
   
   
   // contact form
   $(".firstnameInput").val("First name");
        $(".firstnameInput").addClass("holderText");

        $(".firstnameInput").focus(function() {
            if ($(".firstnameInput").val() == "First name") {
                $(".firstnameInput").removeClass("holderText");
                $(".firstnameInput").val("");
            }
        });

        $(".firstnameInput").blur(function() {
            if ($(".firstnameInput").val() == "") {
                $(".firstnameInput").addClass("holderText");
                $(".firstnameInput").val("First name");
            }
        });
		
	$(".lastnameInput").val("Last name");
        $(".lastnameInput").addClass("holderText");

        $(".lastnameInput").focus(function() {
            if ($(".lastnameInput").val() == "Last name") {
                $(".lastnameInput").removeClass("holderText");
                $(".lastnameInput").val("");
            }
        });

        $(".lastnameInput").blur(function() {
            if ($(".lastnameInput").val() == "") {
                $(".lastnameInput").addClass("holderText");
                $(".lastnameInput").val("Last name");
            }
        });
		
	$(".emailInput").val("Your email");
        $(".emailInput").addClass("holderText");

        $(".emailInput").focus(function() {
            if ($(".emailInput").val() == "Your email") {
                $(".emailInput").removeClass("holderText");
                $(".emailInput").val("");
            }
        });

        $(".emailInput").blur(function() {
            if ($(".emailInput").val() == "") {
                $(".emailInput").addClass("holderText");
                $(".emailInput").val("Your email");
            }
        });
		
		$(".companyInput").val("Company name");
        $(".companyInput").addClass("holderText");

        $(".companyInput").focus(function() {
            if ($(".companyInput").val() == "Company name") {
                $(".companyInput").removeClass("holderText");
                $(".companyInput").val("");
            }
        });

        $(".companyInput").blur(function() {
            if ($(".companyInput").val() == "") {
                $(".companyInput").addClass("holderText");
                $(".companyInput").val("Company name");
            }
        });
		
		$(".websiteInput").val("Company website");
        $(".websiteInput").addClass("holderText");

        $(".websiteInput").focus(function() {
            if ($(".websiteInput").val() == "Company website") {
                $(".websiteInput").removeClass("holderText");
                $(".websiteInput").val("");
            }
        });

        $(".websiteInput").blur(function() {
            if ($(".websiteInput").val() == "") {
                $(".websiteInput").addClass("holderText");
                $(".websiteInput").val("Company website");
            }
        });

        $(".messageText").val("Your message");
        $(".messageText").addClass("holderText");

        $(".messageText").focus(function() {
            if ($(".messageText").val() == "Your message") {
                $(".messageText").removeClass("holderText");
                $(".messageText").val("");
            }
        });

        $(".messageText").blur(function() {
            if ($(".messageText").val() == "") {
                $(".messageText").addClass("holderText");
                $(".messageText").val("Your message");
            }
        });
		
		// click submit button hire quri
	    $('.submit_btn.hire').click(function(e) {
			e.preventDefault();
			var first_name = $(".firstnameInput").val();  
			var last_name = $(".lastnameInput").val();  
			var email = $(".emailInput").val();
			var company = $(".companyInput").val();
			var URL = $(".websiteInput").val();
			var message = $(".messageText").val();
			var dataString = 'first_name='+ first_name + '&last_name=' + last_name + '&email=' + email + '&company=' + company + '&URL=' + URL + '&description=' + message 
			+ '&oid=' + "00Dd0000000beHJ" + '&lead_source=' + "Web Direct" + '&Campaign_ID=' + "701d00000008HBw" + '&00Nd00000051LOF=' + "https://na14.salesforce.com/701d00000008HBw";  
			$.ajax({  
			  type: "POST",  
			  url: "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",  
			  data: dataString,  
			  success: function() {  
				$('.thankyou_message').css('display', 'block');    
			  }  
			});  
			return false;
	    });
		
		// click submit button demo quri
	    /*$('.submit_btn.contact').click(function(e) {
			e.preventDefault();
			var first_name = $(".firstnameInput").val();  
			var last_name = $(".lastnameInput").val();  
			var email = $(".emailInput").val();
			var company = $(".companyInput").val();
			var URL = $(".websiteInput").val();
			var dataString = 'first_name='+ first_name + '&last_name=' + last_name + '&email=' + email + '&company=' + company + '&URL=' + URL + 
			+ '&oid=' + "00Dd0000000beHJ" + '&lead_source=' + "Web Direct" + '&Campaign_ID=' + "701d00000008N4x" + '&00Nd00000051LOF=' + "https://na14.salesforce.com/701d00000008N4x";  
			$.ajax({  
			  type: "POST",  
			  url: "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",  
			  data: dataString,  
			  success: function() {  
				$('.thankyou_message').css('display', 'block');    
			  }  
			});  
			return false;
	    }); */
		
		// click submit demo page
	    $('.submit_btn.contact').click(function(e) {
			e.preventDefault();
			var first_name = $(".firstnameInput").val();  
			var last_name = $(".lastnameInput").val();  
			var email = $(".emailInput").val();
			var company = $(".companyInput").val();
			var dataString = 'first_name='+ first_name + '&last_name=' + last_name + '&email=' + email + '&company=' + company + 
			'&oid=' + "00Dd0000000beHJ" + '&lead_source=' + "Web Direct" + '&Campaign_ID=' + "701d00000008ZbD" + '&00Nd00000051LOF=' + "https://na14.salesforce.com/701d00000008ZbD";  
			$.ajax({  
			  type: "POST",  
			  url: "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",  
			  data: dataString,  
			  success: function() {  
				$('.thankyou_message').css('display', 'block');    
			  }  
			});  
			return false;
	    });
		
		// click submit demo page - new
	    $('.submit_btn.contact_new').click(function(e) {
			e.preventDefault();
			var first_name = $(".firstnameInput").val();  
			var last_name = $(".lastnameInput").val();  
			var email = $(".emailInput").val();
			var company = $(".companyInput").val();
			var dataString = 'first_name='+ first_name + '&last_name=' + last_name + '&email=' + email + '&company=' + company + 
			'&oid=' + "00Dd0000000beHJ" + '&lead_source=' + "Web Direct" + '&Campaign_ID=' + "701d00000008XSq" + '&00Nd00000051LOF=' + "https://na14.salesforce.com/701d00000008XSq";  
			$.ajax({  
			  type: "post",  
			  url: "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",  
			  data: dataString,  
			  success: function() {  
				$('.thankyou_message').css('display', 'block');    
			  }  
			});  
			return false;
	    });
		
		// click submit button qcard page second option
	    /*$('.submit_btn.qcard-2').click(function(e) {
			e.preventDefault();
			var first_name = $(".firstnameInput").val();  
			var last_name = $(".lastnameInput").val();  
			var email = $(".emailInput").val();
			var company = $(".companyInput").val();
			var dataString = 'first_name='+ first_name + '&last_name=' + last_name + '&email=' + email + '&company=' + company + 
			'&oid=' + "00Dd0000000beHJ" + '&lead_source=' + "Web Direct" + '&Campaign_ID=' + "701d00000008XSv" + '&00Nd00000051LOF=' + "https://na14.salesforce.com/701d00000008XSv";  
			$.ajax({  
			  type: "POST",  
			  url: "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",  
			  data: dataString,  
			  success: function() {  
				$('.thankyou_message').css('display', 'block');    
			  }  
			});  
			return false;
	    }); */   
		
		
		
		// privacy
		$('a.privacy_btn').click(function(e) {
			e.preventDefault();
		});
  
});


function autoSlideshow() {
		if ($('a.slide_btn_3').hasClass('current_slide')) {
			// button state
			$('a.slide_btn_3').css('background-position', 'top left').removeClass('current_slide');
			$('a.slide_btn_4').css('background-position', 'top right').addClass('current_slide');
			// scroll to piece
			$('#allSlides').animate({'left':'-1662'}, 'slow');
			// fadein header
			$('h4.title_three').fadeOut();
			$('h4.title_four').delay(400).fadeIn();
		} else if ($('a.slide_btn_2').hasClass('current_slide')) {
			// button state
			$('a.slide_btn_2').css('background-position', 'top left').removeClass('current_slide');
			$('a.slide_btn_3').css('background-position', 'top right').addClass('current_slide');
			// scroll to piece
			$('#allSlides').animate({'left':'-1108'}, 'slow');
			// fadein header
			$('h4.title_two').fadeOut();
			$('h4.title_three').delay(400).fadeIn();
		} else if ($('a.slide_btn_1').hasClass('current_slide')) {
			// button state
			$('a.slide_btn_1').css('background-position', 'top left').removeClass('current_slide');
			$('a.slide_btn_2').css('background-position', 'top right').addClass('current_slide');
			// scroll to piece
			$('#allSlides').animate({'left':'-554'}, 'slow');
			// fadein header
			$('h4.title_one').fadeOut();
			$('h4.title_two').delay(400).fadeIn();
		} else if ($('a.slide_btn_4').hasClass('current_slide')) {
			// button state
			$('a.slide_btn_4').css('background-position', 'top left').removeClass('current_slide');
			$('a.slide_btn_1').css('background-position', 'top right').addClass('current_slide');
			// scroll to piece
			$('#allSlides').animate({'left':'0'}, 'slow');
			// fadein header
			$('h4.title_four').fadeOut();
			$('h4.title_one').delay(400).fadeIn();
		}
		setTimeout(autoSlideshow, 5000);
}